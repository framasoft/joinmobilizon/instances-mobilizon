export interface ServerConfig {
  instanceClientWarning: string;
}

export interface RegisteredExternalAuthConfig {
  npmName: string;
  name: string;
  version: string;
  authName: string;
  authDisplayName: string;
}

export interface RegisteredIdAndPassAuthConfig {
  npmName: string;
  name: string;
  version: string;
  authName: string;
  weight: number;
}

export interface ServerConfig {
  name: string;
  description: string;
  longDescription: string;
  contact: string;

  registrationsOpen: boolean;
  registrationsAllowlist: boolean;
  demoMode: boolean;
  countryCode: string;
  location: {
    latitude: number;
    longitude: number;
    // accuracyRadius: number;
  };
  anonymous: {
    participation: {
      allowed: boolean;
      validation: {
        email: {
          enabled: boolean;
          confirmationRequired: boolean;
        };
        captcha: {
          enabled: boolean;
        };
      };
    };
    eventCreation: {
      allowed: boolean;
      validation: {
        email: {
          enabled: boolean;
          confirmationRequired: boolean;
        };
        captcha: {
          enabled: boolean;
        };
      };
    };
    reports: {
      allowed: boolean;
    };
    actorId: string;
  };
  maps: {
    tiles: {
      endpoint: string;
      attribution: string | null;
    };
  };
  geocoding: {
    provider: string;
    autocomplete: boolean;
  };
  rules: string;
  timezones: string[];
  features: {
    eventCreation: boolean;
    groups: boolean;
  };
  federating: boolean;
  languages: string[];
  version: string;
}
