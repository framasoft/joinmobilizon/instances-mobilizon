export type InstanceFilters = {
  start: number;
  count: number;
  sort: string;
  signup?: string;
  healthy?: string;
  minUserQuota?: number;
  search?: string;
  categoriesOr?: number[];
  languagesOr?: string[];
};
