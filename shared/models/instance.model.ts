import { InstanceStats } from "./instance-stats.model";

export interface Instance extends InstanceStats {
  id: number;
  host: string;

  name: string;
  shortDescription: string;
  version: string;
  signupAllowed: boolean;

  supportsIPv6?: boolean;
  country?: string;

  health: number;

  createdAt: string;

  languages: { displayName: string; code: string }[];
}
