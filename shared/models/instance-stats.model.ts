export interface InstanceStats {
  totalUsers: number;
  totalEvents: number;
  totalLocalEvents: number;
  totalGroups: number;
  totalLocalGroups: number;
  totalInstanceFollowers: number;
  totalInstanceFollowing: number;
}
