export interface GlobalStats {
  totalInstances: number;
  totalEvents: number;
  totalGroups: number;

  totalUsers: number;
}
