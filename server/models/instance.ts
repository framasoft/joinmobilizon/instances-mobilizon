import {
  FindAndCountOptions,
  literal,
  Op,
  QueryTypes,
  WhereOptions,
} from "sequelize";
import {
  AllowNull,
  Column,
  CreatedAt,
  DataType,
  Default,
  Is,
  IsInt,
  Max,
  Model,
  Table,
  UpdatedAt,
} from "sequelize-typescript";
import { InstanceConnectivityStats } from "shared/models/instance-connectivity-stats.model";
import { InstanceFilters } from "shared/models/instance-filters.model";
import { ServerConfig } from "../../shared/models";
import { IStatistics } from "../../shared/models/server/server-stats.model";
import { InstanceHost } from "../../shared/models/instance-host.model";
import { Instance } from "../../shared/models/instance.model";
import { isHostValid } from "../helpers/custom-validators/instances";
import { logger } from "../helpers/logger";
import { INSTANCE_SCORE } from "../initializers/constants";
import { getSort, throwIfNotValid } from "./utils";
import * as express from "express";
import * as inter from "inter";

@Table({
  tableName: "instance",
  indexes: [
    {
      fields: ["host"],
      unique: true,
    },
  ],
})
export class InstanceModel extends Model {
  @AllowNull(false)
  @Is("Host", (value) => throwIfNotValid(value, isHostValid, "valid host"))
  @Column
  host: string;

  @AllowNull(false)
  @Default(INSTANCE_SCORE.MAX)
  @IsInt
  @Max(INSTANCE_SCORE.MAX)
  @Column
  score: number;

  @AllowNull(false)
  @Column(DataType.JSONB)
  stats: IStatistics;

  @AllowNull(false)
  @Column(DataType.JSONB)
  connectivityStats: InstanceConnectivityStats;

  @AllowNull(false)
  @Column(DataType.JSONB)
  config: ServerConfig;

  @AllowNull(false)
  @Default([])
  @Column(DataType.ARRAY(DataType.INTEGER))
  categories: number[];

  @AllowNull(false)
  @Default([])
  @Column(DataType.ARRAY(DataType.TEXT))
  languages: string[];

  @AllowNull(false)
  @Default(false)
  @Column
  blacklisted: boolean;

  @CreatedAt
  createdAt: Date;

  @UpdatedAt
  updatedAt: Date;

  static loadByHost(host: string) {
    const query = {
      where: {
        host,
      },
    };

    return InstanceModel.findOne(query);
  }

  static listForApi(options: InstanceFilters) {
    const query: FindAndCountOptions = {
      offset: options.start,
      limit: options.count,
      order: InstanceModel.getSort(options.sort),
      where: { [Op.and]: InstanceModel.buildWhereFilters(options) },
    };

    return InstanceModel.findAndCountAll(query).then(({ rows, count }) => {
      return {
        data: rows,
        total: count,
      };
    });
  }

  static listForHostsApi(options: InstanceFilters & { since?: string }) {
    const whereAnd = InstanceModel.buildWhereFilters(options);
    if (options.since !== undefined) {
      whereAnd.push({
        createdAt: {
          [Op.gte]: options.since,
        },
      });
    }

    const query: FindAndCountOptions = {
      attributes: ["host", "createdAt"],
      offset: options.start,
      limit: options.count,
      order: InstanceModel.getSort(options.sort),
      where: { [Op.and]: whereAnd },
    };

    return InstanceModel.findAndCountAll(query).then(({ rows, count }) => {
      return {
        data: rows,
        total: count,
      };
    });
  }

  static listHostsWithId() {
    const query = {
      attributes: ["id", "host"],
      where: {
        blacklisted: false,
      },
    };

    return InstanceModel.findAll(query);
  }

  static updateConfigAndStatsAndAbout(
    id: number,
    config: any,
    stats: any,
    connectivityStats: any,
    languages: string[]
  ) {
    const options = {
      where: { id },
    };

    return InstanceModel.update(
      {
        config,
        stats,
        connectivityStats,
        languages,
      },
      options
    );
  }

  static async removeBadInstances() {
    const instances = await InstanceModel.listBadInstances();

    const instancesRemovePromises = instances.map((instance) =>
      instance.destroy()
    );
    await Promise.all(instancesRemovePromises);

    const numberOfInstancesRemoved = instances.length;

    if (numberOfInstancesRemoved)
      logger.info("Removed bad %d instances.", numberOfInstancesRemoved);
  }

  static async updateInstancesScoreAndRemoveBadOnes(
    goodInstances: number[],
    badInstances: number[]
  ) {
    if (goodInstances.length === 0 && badInstances.length === 0) return;

    logger.info(
      "Updating %d good instances and %d bad instances scores.",
      goodInstances.length,
      badInstances.length
    );

    if (goodInstances.length !== 0) {
      await InstanceModel.incrementScores(
        goodInstances,
        INSTANCE_SCORE.BONUS
      ).catch((err) =>
        logger.error("Cannot increment scores of good instances.", err)
      );
    }

    if (badInstances.length !== 0) {
      await InstanceModel.incrementScores(badInstances, INSTANCE_SCORE.PENALTY)
        .then(() => InstanceModel.removeBadInstances())
        .catch((err) =>
          logger.error("Cannot decrement scores of bad instances.", err)
        );
    }
  }

  static async getStats() {
    const queryStats =
      "SELECT " +
      'COUNT(*) as "totalInstances", ' +
      "SUM((stats->>'numberOfUsers')::integer) as \"totalUsers\", " +
      "SUM((stats->>'numberOfLocalEvents')::integer) as \"totalEvents\", " +
      "SUM((stats->>'numberOfLocalGroups')::integer) as \"totalGroups\" " +
      // "SUM((stats->>'totalDailyActiveUsers')::integer) as \"totalDailyActiveUsers\", " +
      // "SUM((stats->>'totalWeeklyActiveUsers')::integer) as \"totalWeeklyActiveUsers\", " +
      // "SUM((stats->>'totalMonthlyActiveUsers')::integer) as \"totalMonthlyActiveUsers\" " +
      'FROM "instance" ' +
      "WHERE blacklisted IS FALSE";

    const queryVersions =
      "SELECT " +
      "(config->>'version') as \"serverVersion\", " +
      'COUNT(*) as "total" ' +
      'FROM "instance" ' +
      "WHERE blacklisted IS FALSE " +
      "GROUP BY (config->>'version')";

    const queryCountries =
      'SELECT COUNT(*) as "total", "connectivityStats"->\'country\' AS "country" ' +
      'FROM "instance" ' +
      "WHERE \"connectivityStats\"->'country' IS NOT NULL " +
      'GROUP BY "country"';

    const promises = [queryStats, queryVersions, queryCountries].map((query) =>
      InstanceModel.sequelize.query(query, { type: QueryTypes.SELECT })
    );

    const [resStats, resVersions, resCountries] = await Promise.all(promises);

    const firstResStats = resStats[0] as any;

    return {
      totalInstances: firstResStats.totalInstances,
      totalUsers: firstResStats.totalUsers,
      totalEvents: firstResStats.totalEvents,
      totalGroups: firstResStats.totalGroups,

      instanceVersions: resVersions.map((v: any) => ({
        serverVersion: v.serverVersion,
        total: v.total,
      })),

      instanceCountries: resCountries.map((c: any) => ({
        countryCode: c.country,
        total: c.total,
      })),
    };
  }

  private static buildWhereFilters(options: InstanceFilters) {
    const whereAnd: WhereOptions[] = [
      {
        blacklisted: false,
      },
    ];

    if (options.healthy !== undefined) {
      const symbol = options.healthy === "true" ? Op.gte : Op.lt;

      whereAnd.push({
        score: {
          [symbol]: INSTANCE_SCORE.HEALTHY_AT,
        },
      });
    }

    if (options.signup !== undefined) {
      whereAnd.push({
        config: {
          signup: {
            allowed: options.signup === "true",
          },
        },
      });
    }

    if (options.search) {
      whereAnd.push({
        host: {
          [Op.iLike]: `%${options.search}%`,
        },
      });
    }

    if (Array.isArray(options.languagesOr)) {
      whereAnd.push({
        languages: {
          [Op.overlap]: options.languagesOr,
        },
      });
    }

    if (Array.isArray(options.categoriesOr)) {
      whereAnd.push({
        categories: {
          [Op.overlap]: options.categoriesOr,
        },
      });
    }

    if (options.minUserQuota) {
      whereAnd.push({
        [Op.or]: [
          {
            config: {
              user: {
                videoQuota: {
                  [Op.gte]: options.minUserQuota,
                },
              },
            },
          },
          {
            config: {
              user: {
                videoQuota: {
                  [Op.eq]: -1,
                },
              },
            },
          },
        ],
      });
    }

    return whereAnd;
  }

  private static listBadInstances() {
    const query = {
      where: {
        score: {
          [Op.lte]: 0,
        },
      },
      logging: false,
    };

    return InstanceModel.findAll(query);
  }

  private static incrementScores(instances: number[], value: number) {
    const instancesString = instances.map((id) => id.toString()).join(",");

    const query =
      `UPDATE "instance" SET "score" = LEAST("score" + ${value}, ${INSTANCE_SCORE.MAX}) ` +
      "WHERE id IN (" +
      instancesString +
      ")";

    const options = {
      type: QueryTypes.BULKUPDATE,
    };

    return InstanceModel.sequelize.query(query, options);
  }

  private static getSort(sort: string) {
    const mappingColumns = {
      totalUsers: literal(`stats->'numberOfUsers'`),
      totalEvents: literal(`stats->'numberOfEvents'`),
      totalLocalEvents: literal(`stats->'numberOfLocalEvents'`),
      totalGroups: literal(`stats->'numberOfGroups'`),
      totalLocalGroups: literal(`stats->'numberOfLocalGroups'`),
      totalInstanceFollowers: literal(`stats->'numberOfInstanceFollowers'`),
      totalInstanceFollowing: literal(`stats->'numberOfInstanceFollowings'`),
      signupAllowed: literal(`config->'registrationsOpen'`),
      name: literal(`config->'name'`),
      version: literal(`config->'version'`),
      health: "score",
      country: literal(`"connectivityStats"->'country'`),
    };

    return getSort(sort, ["id", "ASC"], mappingColumns);
  }

  toFormattedJSON({ locale }: express.Request): Instance {
    return {
      id: this.id,
      host: this.host,

      // config
      name: this.config.name,
      shortDescription: this.config.description,
      version: this.config.version,
      signupAllowed: this.config.registrationsOpen,

      // stats
      totalUsers: this.stats.numberOfUsers,
      totalEvents: this.stats.numberOfEvents,
      totalLocalEvents: this.stats.numberOfLocalEvents,
      totalGroups: this.stats.numberOfGroups,
      totalLocalGroups: this.stats.numberOfLocalGroups,
      totalInstanceFollowers: this.stats.numberOfInstanceFollowers,
      totalInstanceFollowing: this.stats.numberOfInstanceFollowings,

      // connectivity
      supportsIPv6: this.connectivityStats
        ? this.connectivityStats.supportsIPv6
        : undefined,
      country: this.connectivityStats
        ? this.connectivityStats.country
        : undefined,

      languages: this.languages
        .filter(
          (language) =>
            inter.load(locale).getLanguage(language.toLowerCase()) != undefined
        )
        .map((language) => {
          return {
            displayName: inter.load(locale).getLanguage(language.toLowerCase())
              .displayName,
            code: language,
          };
        }),

      // computed stats
      health: Math.round((this.score / INSTANCE_SCORE.MAX) * 100),

      createdAt: this.createdAt.toISOString(),
    };
  }

  toHostFormattedJSON(): InstanceHost {
    return {
      host: this.host,
    };
  }
}
