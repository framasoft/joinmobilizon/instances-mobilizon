import {
  AllowNull,
  BelongsTo,
  Column,
  CreatedAt,
  DataType,
  ForeignKey,
  Model,
  Table,
  UpdatedAt,
} from "sequelize-typescript";
import { InstanceModel } from "./instance";
import * as Sequelize from "sequelize";
import { Order } from "sequelize";
import { MAX_HISTORY_SIZE } from "../initializers/constants";
import { IStatistics } from "shared/models/server";
import * as express from "express";

@Table({
  tableName: "history",
  indexes: [
    {
      fields: ["instanceId"],
    },
    {
      fields: ["createdAt"],
    },
  ],
})
export class HistoryModel extends Model {
  @AllowNull(false)
  @Column(DataType.JSONB)
  stats: IStatistics;

  @CreatedAt
  createdAt: Date;

  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => InstanceModel)
  @Column
  instanceId: number;

  @BelongsTo(() => InstanceModel, {
    foreignKey: {
      allowNull: false,
    },
    onDelete: "cascade",
  })
  Instance: InstanceModel;

  static doesTodayHistoryExist(instanceId: number) {
    const today = new Date();
    today.setHours(0, 0, 0);

    const query =
      'SELECT 1 FROM "history" WHERE "instanceId" = $instanceId AND DATE("createdAt") = CURRENT_DATE LIMIT 1';
    const options = {
      type: Sequelize.QueryTypes.SELECT,
      bind: { instanceId },
      raw: true,
    };

    return HistoryModel.sequelize
      .query(query, options)
      .then((results: any[]) => {
        return results.length === 1;
      });
  }

  static async addEntryIfNeeded(instanceId: number, stats: IStatistics) {
    // Only add 1 entry per day
    const exists = await HistoryModel.doesTodayHistoryExist(instanceId);
    if (exists) return;

    return HistoryModel.create({
      stats,
      instanceId,
    });
  }

  static getInstanceHistory(instanceId: number) {
    const query = {
      order: [["createdAt", "DESC"]] as Order,
      limit: MAX_HISTORY_SIZE,
      where: {
        instanceId,
      },
    };

    return HistoryModel.findAll(query);
  }

  static async getGlobalStats() {
    const query =
      "SELECT " +
      'DATE("history"."createdAt") as "date", ' +
      'COUNT(*) as "totalInstances", ' +
      'SUM(("history".stats->>\'numberOfUsers\')::integer) as "totalUsers", ' +
      'SUM(("history".stats->>\'numberOfLocalEvents\')::integer) as "totalEvents", ' +
      'SUM(("history".stats->>\'numberOfLocalGroups\')::integer) as "totalGroups" ' +
      // 'SUM(("history".stats->>\'totalDailyActiveUsers\')::integer) as "totalDailyActiveUsers", ' +
      // 'SUM(("history".stats->>\'totalWeeklyActiveUsers\')::integer) as "totalWeeklyActiveUsers", ' +
      // 'SUM(("history".stats->>\'totalMonthlyActiveUsers\')::integer) as "totalMonthlyActiveUsers" ' +
      'FROM "history" ' +
      'INNER JOIN "instance" ON "history"."instanceId" = "instance"."id" ' +
      "WHERE instance.blacklisted IS FALSE " +
      'GROUP BY DATE("history"."createdAt") ' +
      'ORDER BY DATE("history"."createdAt") DESC ' +
      "LIMIT " +
      MAX_HISTORY_SIZE;

    return InstanceModel.sequelize
      .query(query, { type: Sequelize.QueryTypes.SELECT })
      .then((results: any[]) =>
        results.map((res) => ({
          date: res.date,
          stats: {
            totalInstances: res.totalInstances,
            totalUsers: res.totalUsers,
            // totalDailyActiveUsers: res.totalDailyActiveUsers,
            // totalWeeklyActiveUsers: res.totalWeeklyActiveUsers,
            // totalMonthlyActiveUsers: res.totalMonthlyActiveUsers,
            totalEvents: res.totalEvents,
            totalGroups: res.totalGroups,
          },
        }))
      );
  }

  toFormattedJSON(req: express.Request) {
    return {
      date: this.createdAt.toISOString().split("T")[0],

      stats: {
        totalUsers: this.stats.numberOfUsers,
        totalEvents: this.stats.numberOfEvents,
        totalLocalEvents: this.stats.numberOfLocalEvents,
        totalGroups: this.stats.numberOfGroups,
        totalLocalGroups: this.stats.numberOfLocalGroups,
        totalInstanceFollowers: this.stats.numberOfInstanceFollowers,
        totalInstanceFollowing: this.stats.numberOfInstanceFollowings,
      },
    };
  }
}
