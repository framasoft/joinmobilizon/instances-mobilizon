import { gql } from "@apollo/client/core";

export const VERSION = gql`
  query {
    config {
      version
    }
  }
`;

export const CONFIG = gql`
  query {
    config {
      name
      description
      longDescription
      registrationsOpen
      registrationsAllowlist
      demoMode
      countryCode
      anonymous {
        participation {
          allowed
          validation {
            email {
              enabled
              confirmationRequired
            }
            captcha {
              enabled
            }
          }
        }
        eventCreation {
          allowed
          validation {
            email {
              enabled
              confirmationRequired
            }
            captcha {
              enabled
            }
          }
        }
        reports {
          allowed
        }
        actorId
      }
      location {
        latitude
        longitude
        # accuracyRadius
      }
      maps {
        tiles {
          endpoint
          attribution
        }
      }
      geocoding {
        provider
        autocomplete
      }
      resourceProviders {
        type
        endpoint
        software
      }
      features {
        groups
        eventCreation
      }
      auth {
        ldap
        oauthProviders {
          id
          label
        }
      }
      languages
      version
    }
  }
`;
