import { gql } from "@apollo/client/core";

export const STATISTICS = gql`
  query {
    statistics {
      numberOfUsers
      numberOfEvents
      numberOfLocalEvents
      numberOfComments
      numberOfLocalComments
      numberOfGroups
      numberOfLocalGroups
      numberOfInstanceFollowings
      numberOfInstanceFollowers
    }
  }
`;
