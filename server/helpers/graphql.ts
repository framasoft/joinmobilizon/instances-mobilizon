import {
  ApolloClient,
  InMemoryCache,
  NormalizedCacheObject,
  HttpLink,
} from "@apollo/client/core";
import fetch from "cross-fetch";

export function initApolloClient(
  uri: string
): ApolloClient<NormalizedCacheObject> {
  return new ApolloClient({
    uri,
    link: new HttpLink({ uri: uri, fetch }),
    cache: new InMemoryCache(),
  });
}
