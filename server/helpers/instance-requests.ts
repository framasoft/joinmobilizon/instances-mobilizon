import { ServerConfig } from "../../shared/models";
import { IStatistics } from "../../shared/models/server/server-stats.model";
import { InstanceConnectivityStats } from "shared/models/instance-connectivity-stats.model";
import { countryLookup } from "../initializers/geoip";
import { faultTolerantResolve } from "./utils";
import { initApolloClient } from "./graphql";
import { CONFIG, VERSION } from "../graphql/config";
import { STATISTICS } from "../graphql/statistics";

async function getInstanceVersion(host: string): Promise<string> {
  const apollo = initApolloClient(`https://${host}/api`);
  const result = await apollo.query({ query: VERSION });
  return result.data.config.version;
}

async function fetchInstanceConfig(host: string): Promise<ServerConfig> {
  const apollo = initApolloClient(`https://${host}/api`);
  const result = await apollo.query({ query: CONFIG });
  return result.data.config;
}

async function fetchInstanceStats(host: string) {
  const apollo = initApolloClient(`https://${host}/api`);
  const result = await apollo.query({ query: STATISTICS });
  const ipv4Addresses = await faultTolerantResolve(host, "A");
  const ipv6Addresses = await faultTolerantResolve(host, "AAAA");
  const addresses = [...ipv4Addresses, ...ipv6Addresses];

  const supportsIPv6 = ipv6Addresses.length > 0;

  const country = await getCountryFromAddresses(addresses);

  return {
    stats: result.data.statistics as IStatistics,
    connectivityStats: {
      supportsIPv6,
      country,
    } as InstanceConnectivityStats,
  };
}

async function getCountryFromAddresses(addresses: string[]): Promise<string> {
  if (addresses.length > 0) {
    const lookup = await countryLookup(addresses[0]);
    if (lookup) {
      return lookup.country?.iso_code;
    }
  }
  return undefined;
}

async function getConfigAndStatsAndAboutInstance(host: string) {
  const [config, { stats, connectivityStats }] = await Promise.all([
    fetchInstanceConfig(host),
    fetchInstanceStats(host),
  ]);

  if (
    !config ||
    !stats ||
    config.version === undefined ||
    stats.numberOfEvents === undefined
  ) {
    throw new Error(
      "Invalid remote host. Are you sure this is a Mobilizon instance?"
    );
  }

  return { config, stats, connectivityStats, languages: config.languages };
}

// ---------------------------------------------------------------------------

export {
  getConfigAndStatsAndAboutInstance,
  fetchInstanceConfig,
  fetchInstanceStats,
  getInstanceVersion,
};
