# Application behind instances.joinmobilizon.org

## Dev

```terminal
$ yarn install --pure-lockfile
```

Initialize the database:

```terminal
$ sudo -u postgres createuser -P mobilizon
$ sudo -u postgres createdb -O mobilizon mobilizon_instances
```

Then run simultaneously (for example with 3 terminals):

```terminal
$ tsc -w
```

```terminal
$ node dist/server
```

```terminal
$ cd client && npm run serve
```

Then open http://localhost:8080.

## Production

In the root of the cloned repo:

```terminal
$ yarn install --pure-lockfile
$ npm run build
$ sudo -u postgres createuser -P mobilizon
$ sudo -u postgres createdb -O mobilizon mobilizon_instances
$ node dist/server.js
```

